/**
 * 
 */
package pruebasDAO;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 19 ene 2023
 */
public class Ataque {

	private String id;
	private String nombre;
	private String tipo;
	private int velocidad;

	public Ataque() {
	}
	
	public Ataque(String nombre, String tipo, int velocidad) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
		this.velocidad = velocidad;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	@Override
	public String toString() {
		return "Ataque [nombre=" + nombre + ", tipo=" + tipo + ", velocidad=" + velocidad + "]";
	}
	
	
	
	
}
