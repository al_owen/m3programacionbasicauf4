/**
 * 
 */
package pruebasDAO;

import funciones.UtilConsole;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 12 ene 2023
 */
public class IniciVistaControlador {
	
	public static void main(String[] args) {
		PokemonDao pd = new PokemonDao();
		AtaquesDao ad = new AtaquesDao();
		
		int opcion = 0;
		do {
			//menu
			System.out.println("0. Salir");
			System.out.println("1. Crear pokemon");
			System.out.println("2. Borrar pokemon");
			System.out.println("3. Mostrar pokemons");
			System.out.println("4. buscar pokemons");
			opcion = UtilConsole.pedirInt();
			switch (opcion) {
			case 0:
				return;
			case 1:
				crearPokemons(pd, ad);
				break;
			case 2:
				break;
			case 3: 
				break;
			case 4:
				break;
			default:
				break;
			}

		} while (opcion != 0);
	}

	/**
	 * @param pd
	 * @param ad 
	 */
	private static void crearPokemons(PokemonDao pd, AtaquesDao ad) {
		System.out.println("Escribe el id del pokemon");
		String id = UtilConsole.pedirString();
		System.out.println("Escribe el nombre ");
		String nombre = UtilConsole.pedirString();
		System.out.println("Escribe el tipo ");
		String tipo = UtilConsole.pedirString();
		System.out.println("Escribe el hp ");
		int hp = UtilConsole.pedirInt();
		System.out.println("Escribe el ataque ");
		int atck = UtilConsole.pedirInt();
		System.out.println("Escribe la defensa ");
		int def = UtilConsole.pedirInt();
		Pokemon p = new Pokemon(id, nombre, tipo, hp, atck, def);
		System.out.println("Introduce el id de ataque");
		int ind = UtilConsole.pedirInt();
		p.setAtaques(ad.buscar(id), UtilConsole.pedirInt());
		pd.guardar(p);
		Object obj =  new Object();
		
	}

}
