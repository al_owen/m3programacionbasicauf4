/**
 * 
 */
package pruebasDAO;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 19 ene 2023
 */
public class Pokemon {

	private String id;
	private String nombre;
	private String tipo;
	private int hp;
	private int atck;
	private int def;
	private Ataque[] ataques;
	
	public Pokemon() {
	}
	
	public Pokemon(String id, String nombre, String tipo, int hp, int atck, int def) {
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.hp = hp;
		this.atck = atck;
		this.def = def;
	}
	
	public Ataque[] getAtaques() {
		return ataques;
	}

	public void setAtaques(Ataque ataque, int index) {
		this.ataques[index] = ataque;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAtck() {
		return atck;
	}

	public void setAtck(int atck) {
		this.atck = atck;
	}

	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	@Override
	public String toString() {
		return "Pokemon [nombre=" + nombre + ", tipo=" + tipo + ", hp=" + hp + ", atck=" + atck + ", def=" + def + "]";
	}
	
	
	
	
}
