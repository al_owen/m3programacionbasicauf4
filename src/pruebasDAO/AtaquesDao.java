/**
 * 
 */
package pruebasDAO;

import java.util.HashMap;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 19 ene 2023
 */
public class AtaquesDao {

private HashMap<String, Ataque> ataques = new HashMap<>();
	
	public void guardar(Ataque p) {
		ataques.put(p.getId(), p);
	}
	
	public Ataque buscar(String id) {
		if (ataques.containsKey(id)) {
			return ataques.get(id);
		}
		return null;
	}
	
	public Ataque eliminar(String id) {
		if (ataques.containsKey(id)) {
			Ataque p = ataques.get(id);
			ataques.remove(id);
			return p;
		}
		return null;
	}
	
	public void mostrar() {
		if (!ataques.isEmpty()) {
			for (Ataque pokemon : ataques.values()) {
				System.out.println(pokemon);
			}
		}else {
			System.out.println("La pokedex esta vacia!");
		}
	}
	
}
