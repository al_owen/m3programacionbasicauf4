/**
 * 
 */
package pruebasDAO;

import java.util.HashMap;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 19 ene 2023
 */
public class PokemonDao {

	private HashMap<String, Pokemon> pokedex = new HashMap<>();
	
	public void guardar(Pokemon p) {
		pokedex.put(p.getId(), p);
	}
	
	public Pokemon buscar(String id) {
		if (pokedex.containsKey(id)) {
			return pokedex.get(id);
		}
		return null;
	}
	
	public Pokemon eliminar(String id) {
		if (pokedex.containsKey(id)) {
			Pokemon p = pokedex.get(id);
			pokedex.remove(id);
			return p;
		}
		return null;
	}
	
	public void mostrar() {
		if (!pokedex.isEmpty()) {
			for (Pokemon pokemon : pokedex.values()) {
				System.out.println(pokemon);
			}
		}else {
			System.out.println("La pokedex esta vacia!");
		}
	}
	
}
