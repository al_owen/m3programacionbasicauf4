/**
 * 
 */
package herencia;

import interfaces.Volador;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 6 feb 2023
 */
public class Murcielago extends Mamifero implements Volador{

	/**
	 * @param nombre
	 * @param altura
	 * @param peso
	 * @param edad
	 * @param daLeche
	 */
	public Murcielago(String nombre, double altura, double peso, int edad, boolean daLeche) {
		super(nombre, altura, peso, edad, daLeche);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void volar() {
		System.out.println("Vuela con sus alas");
		
	}

}
