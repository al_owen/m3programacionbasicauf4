/**
 * 
 */
package herencia;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 24 ene 2023
 */
//La clase extiende de Animal. Esto se especifica mediante la palabra reservada 'extends'
public abstract class Mamifero extends Animal {
	
	//atributos de mamifero
	private boolean daLeche;

	/**
	 * @param nombre
	 * @param altura
	 * @param peso
	 * @param edad
	 */
	public Mamifero(String nombre, double altura, double peso, int edad, boolean daLeche) {
		//super llama al constructor del padre, en este caso, "Animal"
		super(nombre, altura, peso, edad);
		//guardamos en los atributos correspondientes, el resto de atributos que solo pertenecen a mamifero
		this.daLeche = daLeche;
	}
	
	
}
