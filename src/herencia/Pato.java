/**
 * 
 */
package herencia;

import interfaces.Nadador;
import interfaces.Volador;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 6 feb 2023
 */
public class Pato extends Ave implements Nadador{

	/**
	 * @param nombre
	 * @param altura
	 * @param peso
	 * @param edad
	 */
	public Pato(String nombre, double altura, double peso, int edad) {
		super(nombre, altura, peso, edad);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void nadar() {
		System.out.println("Nado");
		
	}

	public void volar() {
		System.out.println("vuelo");		
	}

}
