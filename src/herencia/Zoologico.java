/**
 * 
 */
package herencia;

import java.awt.Color;

import interfaces.Volador;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 24 ene 2023
 */
public class Zoologico {

	public static void main(String[] args) {
		//el polimorfismo permite que se guarde un objeto de clase Perro 
		//dentro de una variable de tipo Animal,
		//porque el perro hereda de Animal, por lo tanto es tan Animal como Perro
		Animal an = new Perro();
		//si utilizamos una variable de tipo Animal, solo podremos utilizar los metodos declarados en Animal
		an.comer();
		an.comunicarse();
		//no podemos usar metodos creados en Perro
		//an.moverCola();
		
		
		//creo una variable de tipo perro
		Perro pedro = new Perro("Pedro el perro",2.2, 5.7, 3, Color.BLACK, "Dogo Argentino", true );
		/*En cambio si creo un objeto de tipo perro y lo guardo dentro de una variable de tipo perro
		podré utilizar todos los metodos, tanto heredados, como propios de "Perro"*/
		pedro.moverLaCola();
		//tambien podemos acceder a atributos heredados, mediante los getters y setters 
		
		System.out.println("Me llamo "+pedro.getNombre());
		Animal a = new Perro();
		soltarAnimales(a);
	}

	/**
	 * 
	 */
	private static void soltarAnimales(Animal obj) {
		if (obj instanceof Volador) {
			((Volador)obj).volar();
		}
		
	}
	
}
