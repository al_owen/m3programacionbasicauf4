/**
 * 
 */
package herencia;

import interfaces.Volador;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 6 feb 2023
 */
public class Halcon extends Ave{

	/**
	 * @param nombre
	 * @param altura
	 * @param peso
	 * @param edad
	 */
	public Halcon(String nombre, double altura, double peso, int edad) {
		super(nombre, altura, peso, edad);
		// TODO Auto-generated constructor stub
	}

	
	public void volar() {
		System.out.println("Vuela muy alto");
	}

}
