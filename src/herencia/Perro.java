/**
 * 
 */
package herencia;

import java.awt.Color;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 24 ene 2023
 */
public class Perro extends Mamifero{

	//atributos
	private Color colorPelaje;
	private String raza;
	private boolean ppp;
	
	/**
	 * @param nombre
	 * @param altura
	 * @param peso
	 * @param edad
	 * @param daLeche
	 */
	public Perro(String nombre, double altura, double peso, int edad, Color colorPelaje, String raza, boolean ppp) {
		//llamamos al constructor de la clase padre "Mamifero"
		super(nombre, altura, peso, edad, true);
		//guardamos el resto de atributos
		this.colorPelaje = colorPelaje;
		this.raza = raza;
		this.ppp = ppp;
	}

	/**
	 * Si no pasamos ningun valor al constructor, deberemos pasar datos por defecto al super,
	 *  ya que el super llama al padre (la superclase), y el constructor dicta los datos necesarios
	 */
	public Perro() {
		super("", 0.0, 0.0, 0, true);
	}

	//sobreescribimos algunos metodos, en este caso de Animal, pero tambien se puede de Mamifero
	@Override
	public void comer() {
		super.comer();
		System.out.println("Que rico hueso");
	}

	@Override
	public void comunicarse() {
		System.out.println("Guau Guau");
	}
	
	//funcion unica de perro
	public void moverLaCola() {
		System.out.println("Muevo la cola");
	}
	
}
