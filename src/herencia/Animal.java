/**
 * 
 */
package herencia;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 24 ene 2023
 */
public abstract class Animal {

	//atributos de los animales
	private String nombre;
	private double altura;
	private double peso;
	private int edad;
	
	//constructor 
	public Animal(String nombre, double altura, double peso, int edad) {
		this.nombre = nombre;
		this.altura = altura;
		this.peso = peso;
		this.edad = edad;
	}

	//getters y setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	//metodos que se sobreescriben
	@Override
	public String toString() {
		return "Animal [nombre=" + nombre + ", altura=" + altura + ", peso=" + peso + ", edad=" + edad + "]";
	}
	
	public void comer() {
		System.out.println("ñam ñam");
	}
	
	public void moverse() {
		System.out.println("me muevo");
	}
	
	public void comunicarse() {
		System.out.println("Hola (en animal)");
	}
	
}
