/**
 * 
 */
package projecteEx;

import java.util.HashMap;

/**
 * @author Álvaro Owen de la Quintana ProgramacionBasicaUF4 26 ene 2023
 */
public class ProductesDao implements Persistable{

	private HashMap<Integer, Object> productes = new HashMap<>();
	
	public void afegir(ProducteAbstract prod) {
		productes.put(prod.getId(), prod);
	}

	public Object buscar(int id) {
		return productes.get(id);
	}

	@Override
	public Object guardar(Object obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object eliminar(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Integer, Object> getMap() {
		return productes;
	}
	
}
