package projecteEx;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 26 ene 2023
 */
public class Producte extends ProducteAbstract {
	private int stock;
	private double precio;
	
	/**
	 * 
	 * @param id
	 * @param nom
	 * @param stock
	 * @param precio
	 */
	public Producte(int id, String nom, int stock, double precio) {
		super(id, nom);
		this.stock = stock;
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Producte [stock=" + stock + ", precio=" + precio + ", Id=" + getId() + ", Nom=" + getNom()
				+ "]";
	}

	
	
}
