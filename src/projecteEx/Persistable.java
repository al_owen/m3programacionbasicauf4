/**
 * 
 */
package projecteEx;

import java.util.HashMap;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 6 feb 2023
 */
public interface Persistable {

	//guardar
	public Object guardar(Object obj);
	//eliminar
	public Object eliminar(int id);
	//buscar
	public Object buscar(int id);
	//getMap
	public HashMap<Integer, Object> getMap();
}
