package genericos;

import java.util.ArrayList;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 7 feb 2023
 */
public class Pool <T>{

	ArrayList<T> pool = new ArrayList<>();
	
	
	
	public Pool(int size) {
		//creamos la cantidad que haga falta
	}

	public T getObject() {
		T temp = pool.get(0);
		pool.remove(0);
		return temp;
	}
	
	public void returnObject(T obj) {
		pool.add(obj);
	}
	
	
}
