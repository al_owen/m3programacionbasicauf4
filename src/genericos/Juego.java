/**
 * 
 */
package genericos;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 7 feb 2023
 */
public class Juego {

	public static void main(String[] args) {
		Pool<Enemies> enemies = new Pool<Enemies>(20);
		Pool<Bala> balas = new Pool<>(30);
		
		Bala b = balas.getObject();
		Enemies e = enemies.getObject();
		
		balas.returnObject(b);
		enemies.returnObject(e);
	}
	
}
