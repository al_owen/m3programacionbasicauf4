/**
 * 
 */
package tests;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 13 feb 2023
 */
public class Pokemon {
	 private String name;
	    private int hp;
	    private int attack;
	    private int defense;

	    public Pokemon(String name, int hp, int attack, int defense) {
	        this.name = name;
	        this.hp = hp;
	        this.attack = attack;
	        this.defense = defense;
	    }

	    public String getName() {
	        return name;
	    }

	    public int getHp() {
	        return hp;
	    }

	    public int attack() {
	        return attack;
	    }

	    public void takeDamage(int damage) {
	        hp -= damage;
	    }
}
