/**
 * 
 */
package tests;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 13 feb 2023
 */
public class UserForm {

	 public static void main(String[] args) {
	        JFrame frame = new JFrame("Pokemon Battle");

	        // Initialize Pokemon and their attributes
	        Pokemon pikachu = new Pokemon("Pikachu", 50, 20, 30);
	        Pokemon charmander = new Pokemon("Charmander", 40, 25, 35);

	        // Show initial Pokemon stats in JOptionPane
	        String message = pikachu.getName() + " (HP: " + pikachu.getHp() + ")\n" +
	                         charmander.getName() + " (HP: " + charmander.getHp() + ")";
	        JOptionPane.showMessageDialog(frame, message, "Pokemon Battle", JOptionPane.INFORMATION_MESSAGE);

	        // Start battle
	        while (pikachu.getHp() > 0 && charmander.getHp() > 0) {
	            // Pikachu's turn
	            int damage = pikachu.attack();
	            charmander.takeDamage(damage);
	            message = pikachu.getName() + " attacks " + charmander.getName() + " with Thunderbolt!\n" +
	                      "It deals " + damage + " damage.\n" +
	                      charmander.getName() + " has " + charmander.getHp() + " HP left.";
	            JOptionPane.showMessageDialog(frame, message, "Pokemon Battle", JOptionPane.INFORMATION_MESSAGE);

	            if (charmander.getHp() <= 0) {
	                message = charmander.getName() + " faints. " + pikachu.getName() + " wins!";
	                JOptionPane.showMessageDialog(frame, message, "Pokemon Battle", JOptionPane.INFORMATION_MESSAGE);
	                break;
	            }

	            // Charmander's turn
	            damage = charmander.attack();
	            pikachu.takeDamage(damage);
	            message = charmander.getName() + " attacks " + pikachu.getName() + " with Flamethrower!\n" +
	                      "It deals " + damage + " damage.\n" +
	                      pikachu.getName() + " has " + pikachu.getHp() + " HP left.";
	            JOptionPane.showMessageDialog(frame, message, "Pokemon Battle", JOptionPane.INFORMATION_MESSAGE);

	            if (pikachu.getHp() <= 0) {
	                message = pikachu.getName() + " faints. " + charmander.getName() + " wins!";
	                JOptionPane.showMessageDialog(frame, message, "Pokemon Battle", JOptionPane.INFORMATION_MESSAGE);
	                break;
	            }

	            // Ask user if they want to continue
	            int option = JOptionPane.showConfirmDialog(frame, "Do you want to continue?", "Pokemon Battle", JOptionPane.YES_NO_OPTION);

	            if (option == JOptionPane.NO_OPTION) {
	                break;
	            }
	        }

	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.pack();
	        frame.setVisible(true);
	    }
}
