/**
 * 
 */
package pruebas;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 13 feb 2023
 */
public interface Persistable <T>{

	public void guardar(T obj);
	public T buscar(int id);
	
}
