/**
 * 
 */
package pruebas;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 13 feb 2023
 */
public class IniciVistaController {

	public static void main(String[] args) {
		ProductesDao pdao = new ProductesDao();
		ProductesVistaController pvc = new ProductesVistaController(pdao);
		//menu
		//opcion 1
		pvc.inicio();
	}
	
}
