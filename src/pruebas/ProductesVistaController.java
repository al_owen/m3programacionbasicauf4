/**
 * 
 */
package pruebas;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 13 feb 2023
 */
public class ProductesVistaController {

	private ProductesDao pdao;

	public ProductesVistaController(ProductesDao pdao) {
		this.pdao = pdao;
	}
	
	public void inicio() {
		//submenu
		//opcion 1
		agregar();
	}
	
	public void agregar(){
		Persona p = new Persona(0, "Al");
		pdao.guardar(p);
	}
}
