/**
 * 
 */
package interfaces;

/**@author Álvaro Owen de la Quintana
 * ProgramacionBasicaUF4 
 * 6 feb 2023
 */
public interface Nadador {

	public void nadar();
}
